<?php

namespace Bowling;

Class Game {

     private $scores = [];

     private $total;
    /**
     * @var int
     */
    private $rounds;

    function __construct(int $rounds)
    {
        $this->rounds = $rounds;
    }

    public function calculateFinalScore(...$scores) {
        $this->validate($scores);
        $isStrike = false;

        foreach ($scores as $score) {
            $this->total += $isStrike ? 2 * $score : $score;

            $isStrike = 10 === $score;
        }

        return $this->total;
    }

    private function validate(array $scores)
    {
        if (count($scores) !== 10) {
            throw new \Exception('invalid number');
        }
    }
}
